import PromiseRouter from "express-promise-router";
import {UserModule} from "../modules/user/user-module";
import {IUser} from "../modules/user/user-model";
import {NextFunction, Request, Response} from "express";
import * as jwt from "jsonwebtoken";

export function createIndexRoute() {
    const indexRoute = PromiseRouter();

    async function authenticateToken(req: Request, res: Response, next: NextFunction) {
        const token = req.header("token") == "undefined" ? null : req.header("token");
        if (token) {
            req.body.phoneNumber = jwt.verify(token, process.env.JWT_SECRET_KEY as string);
            next();
        } else {
            res.status(500).json("no token");
        }
    }

    indexRoute.get('/isAlive', function (req, res, next) {
        res.status(200).json();
    });

    indexRoute.post('/sendVerificationCode', async function (req, res, next) {
        const phoneNumber: string = req.body['phone'];

        await UserModule.sendVerificationCode(phoneNumber);

        res.status(200).json();
    });

    indexRoute.post('/verifyCode', async function (req, res, next) {
        const phoneNumber: string = req.body['phone'];
        const code: string = req.body['code'];

        try {
            const userAndToken = await UserModule.verifyCode(phoneNumber, code);
            res.status(200).json(userAndToken);
        } catch (e) {
            res.status(500).json(e);
        }
    });

    indexRoute.get('/getUser', authenticateToken, async function (req, res, next) {
        try {
            const user: IUser = await UserModule.getUser(req.body.phoneNumber);
            res.status(200).json(user);
        } catch (e) {
            res.status(500).json(e);
        }
    });

    indexRoute.post('/updateUserDetails', authenticateToken, async function (req, res, next) {
        const user: IUser = req.body['user'];

        if (req.body.phoneNumber === user.phoneNumber) {
            try {
                const updatedUser: IUser = await UserModule.updateUserDetails(user);
                res.status(200).json(updatedUser);
            } catch (e) {
                res.status(500).json(e);
            }
        }
        res.status(500).json("tryna hack me?");
    });

    return indexRoute;
}

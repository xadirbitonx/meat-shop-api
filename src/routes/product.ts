import PromiseRouter from "express-promise-router";
import {ProductModule} from "../modules/product/product-module";
import {IProduct} from "../modules/product/product-model";

export function createProductRoute() {
    const productRoute = PromiseRouter();

    productRoute.get('/getAllProducts', async function (req, res, next) {
        const products = await ProductModule.getAllProducts();
        res.status(200).json(products);
    });

    productRoute.post('/saveProduct', async function (req, res, next) {
        const product: IProduct = req.body['product'];

        await ProductModule.saveProduct(product);
        res.status(200).json();
    });

    productRoute.delete('/deleteProduct/:productName', async function (req, res, next) {
        const productName = req.params.productName;

        await ProductModule.deleteProduct(productName);
        res.status(200).json();
    });

    return productRoute;
}

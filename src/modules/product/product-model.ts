import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class ProductModel {
    private static model: Model<IProduct>;

    public constructor() {
        ProductModel.model = mongoose.model<IProduct>('products', productSchema);
    }

    public async getAllProducts(): Promise<IProduct[]> {
        return ProductModel.model.find().exec();
    }

    public async saveProduct(product: IProduct): Promise<void> {
        await ProductModel.model.create(product);
    }

    public async deleteProduct(productName: string): Promise<void> {
        await ProductModel.model.findOneAndDelete({name: productName});
    }
}

export interface IProduct{
    name: string,
    priceForKilo: number,
    category: string,
    image: string,
    slicingOptions: string[]
}

const productSchema = new Schema<IProduct>({
    name: String,
    priceForKilo: Number,
    category: String,
    image: String,
    slicingOptions: [String]
});

import {IProduct, ProductModel} from "./product-model";


export class ProductModule {
    private static productModel: ProductModel;

    public static init() {
        ProductModule.productModel = new ProductModel();
    }

    public static async getAllProducts(): Promise<IProduct[]> {
        return await ProductModule.productModel.getAllProducts();
    }

    public static async saveProduct(product: IProduct): Promise<void> {
        await ProductModule.productModel.saveProduct(product);
    }

    public static async deleteProduct(productName: string): Promise<void> {
        await ProductModule.productModel.deleteProduct(productName);
    }
}
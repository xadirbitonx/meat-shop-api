import {IUser, UserModel} from "./user-model";
import twilio, {Twilio} from "twilio";
import * as jwt from "jsonwebtoken";

export class UserModule {
    private static serviceSID: string;
    private static jwtKey: string;
    private static userModel: UserModel;
    private static client: Twilio;

    public static init() {
        UserModule.userModel = new UserModel();
        UserModule.client = twilio(process.env.TWILIO_accountSID, process.env.TWILIO_authToken);
        UserModule.serviceSID = process.env.TWILIO_serviceSID ? process.env.TWILIO_serviceSID : ""
        UserModule.jwtKey = process.env.JWT_SECRET_KEY ? process.env.JWT_SECRET_KEY : "";
    }

    public static async sendVerificationCode(phoneNumber: string): Promise<void> {
        const formattedPhoneNumber = UserModule.formatPhoneNumber(phoneNumber);

        await (UserModule.client.verify.services(UserModule.serviceSID)
            .verifications
            .create({to: formattedPhoneNumber, channel: 'sms'}));
    }

    public static async verifyCode(phoneNumber: string, code: string): Promise<{ user: IUser, token: string }> {
        const formattedPhoneNumber = UserModule.formatPhoneNumber(phoneNumber);

        const response = await UserModule.client.verify.services(UserModule.serviceSID)
            .verificationChecks
            .create({to: formattedPhoneNumber, code: code});

        if (response.status === "approved") {
            const user: IUser = await UserModule.savePhoneNumberIfNotAlreadySaved(phoneNumber);
            const token = jwt.sign(phoneNumber, UserModule.jwtKey);
            return {user: user, token: token};
        }

        throw Error("NOT APPROVED");
    }

    private static async savePhoneNumberIfNotAlreadySaved(phoneNumber: string): Promise<IUser> {
        return await UserModule.userModel.savePhoneNumberIfNotAlreadySaved(phoneNumber);
    }

    public static async getUser(phoneNumber: string): Promise<IUser> {
        return await UserModule.userModel.getUser(phoneNumber);
    }

    public static async updateUserDetails(user: IUser): Promise<IUser> {
        return await UserModule.userModel.updateUserDetails(user);
    }

    private static formatPhoneNumber(phoneNumber: string): string {
        return "+972" + phoneNumber.slice(1);
    }
}
import {Model, Schema} from "mongoose";
import * as mongoose from "mongoose";

export class UserModel {
    private static model: Model<IUser>;

    public constructor() {
        UserModel.model = mongoose.model<IUser>('users', userSchema);
    }

    public async savePhoneNumberIfNotAlreadySaved(phoneNumber: string): Promise<IUser> {
        return await UserModel.model.findOneAndUpdate({phoneNumber: phoneNumber}, {phoneNumber: phoneNumber}, {upsert: true, new: true}).exec();
    }

    public async getUser(phoneNumber: string): Promise<IUser> {
        const user = await UserModel.model.findOne({phoneNumber: phoneNumber}).exec();

        if (user) {
            return user;
        }
        throw Error("no user");
    }

    public async updateUserDetails(user: IUser): Promise<IUser> {
        const updatedUser: IUser | null = await UserModel.model.findOneAndUpdate({phoneNumber: user.phoneNumber}, user, {new: true}).exec();

        if (updatedUser) {
            return updatedUser;
        }
        throw Error("no user");
    }
}

export interface IUser {
    phoneNumber: string,
    firstName?: string,
    lastName?: string,
    address?: string,
    addressNumber?: number,
    city?: string
}

const userSchema = new Schema<IUser>({
    phoneNumber: {type: String, required: true, index: true},
    firstName: {type: String, required: false},
    lastName: {type: String, required: false},
    address: {type: String, required: false},
    addressNumber: {type: Number, required: false},
    city: {type: String, required: false}
}, {
    minimize: false
});
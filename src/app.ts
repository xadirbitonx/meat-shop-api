import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import {UserModule} from "./modules/user/user-module";
import {ProductModule} from "./modules/product/product-module";
import {createProductRoute} from "./routes/product";
import {createIndexRoute} from "./routes";
dotenv.config();

function getApp() {
    const app = express();

    const corsOptions = {
        origin: 'http://localhost:4200',
        optionsSuccessStatus: 200
    };
    app.use(cors(corsOptions));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));

    // Routes
    app.use('/', createIndexRoute());
    app.use('/product', createProductRoute());

    return app;
}

async function initServer(): Promise<number> {
    // connect to db
    await mongoose.connect('mongodb://localhost/meat-shop');
    console.log("connected to db");

    // init modules
    UserModule.init();
    ProductModule.init();

    return 5000;
}

async function startApp(): Promise<void> {
    const port = await initServer();
    const app = getApp();

    app.listen(port, () => {
        console.log("listening on port " + port);
    });
}

startApp().catch(err => {
    console.log("fuck");
    process.exit(1);
});
